from tastypie import fields
from tastypie.resources import ModelResource
from tbr.models import (Activity, ActivityGroup, ActivityMedia, ActivityPackage,
    ActivityPackagePrice, Vendor, Screensaver, Event, Marker, Media)
from sorl.thumbnail import get_thumbnail
from django.utils import timezone


class VendorResource(ModelResource):
    class Meta:
        queryset = Vendor.objects.filter(is_active=True)
        resource_name = 'vendor'


class ActivityPackagePriceResource(ModelResource):
    class Meta:
        queryset = ActivityPackagePrice.objects.filter(is_active=True)
        resource_name = 'activity_package_price'


class ActivityPackageResource(ModelResource):
    prices = fields.ToManyField(
        ActivityPackagePriceResource,
        'activitypackageprice_set', full=True)

    class Meta:
        queryset = ActivityPackage.objects.filter(is_active=True)
        resource_name = 'activity_package'


class ActivityGroupResource(ModelResource):
    class Meta:
        queryset = ActivityGroup.objects.filter(is_active=True)
        resource_name = 'activity_group'


class ActivityMediaResource(ModelResource):
    type = fields.CharField(attribute='type', readonly=True)
    icon = fields.FileField()
    thumb = fields.FileField()

    def dehydrate_icon(self, bundle):
        icon = bundle.obj.activity.group.icon
        if icon:
            return icon.url
        else:
            return ''

    def dehydrate_thumb(self, bundle):
        img = get_thumbnail(bundle.obj.image, '90x50', quality=80)
        return img.url

    class Meta:
        queryset = ActivityMedia.objects.filter(
            is_active=True,
            is_featured=True)
        resource_name = 'activity_media'


class ActivityResource(ModelResource):
    group = fields.ForeignKey(ActivityGroupResource, 'group', full=True)
    vendor = fields.ForeignKey(VendorResource, 'vendor', full=True)
    latitude = fields.CharField(attribute='latitude', readonly=True,
        null=True)
    longitude = fields.CharField(attribute='longitude', readonly=True,
        null=True)
    packages = fields.ToManyField(
        ActivityPackageResource,
        attribute=lambda bundle: ActivityPackage.objects.filter(
            activity=bundle.obj, is_active=True),
        full=True, use_in='detail')
    medias = fields.ToManyField(
        ActivityMediaResource,
        'activitymedia_set', full=True, use_in='detail')

    class Meta:
        queryset = Activity.objects.filter(is_active=True)
        resource_name = 'activity'


class MediaResource(ModelResource):
    type = fields.CharField(attribute='type', readonly=True)
    thumb = fields.FileField()

    def dehydrate_thumb(self, bundle):
        img = get_thumbnail(bundle.obj.image, '175x175', crop='center',
            quality=80)
        return img.url

    class Meta:
        queryset = Media.objects.filter(is_active=True)
        resource_name = 'media'


class BareActivityResource(ModelResource):
    class Meta:
        queryset = Activity.objects.filter(is_active=True)
        resource_name = 'bare_activity'


class ScreensaverResource(ModelResource):
    class Meta:
        queryset = Screensaver.objects.filter(is_active=True)
        resource_name = 'screensaver'


class EventResource(ModelResource):
    class Meta:
        now = timezone.now()
        queryset = Event.objects.filter(is_active=True,
            start_datetime__gt=now).order_by('start_datetime')
        resource_name = 'event'


class MarkerResource(ModelResource):
    activity = fields.ForeignKey(BareActivityResource, 'activity',
        null=True, full=True)
    latitude = fields.CharField(attribute='latitude',
        readonly=True, null=True)
    longitude = fields.CharField(attribute='longitude',
        readonly=True, null=True)
    image_latitude = fields.CharField(attribute='image_latitude',
        readonly=True, null=True)
    image_longitude = fields.CharField(attribute='image_longitude',
        readonly=True, null=True)

    class Meta:
        queryset = Marker.objects.filter(is_active=True)
        resource_name = 'marker'