from django.db import models

# Create your models here.

class BaseModel(models.Model):
    order = models.IntegerField(default=0)
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True
        ordering = ['order', 'name']


class ActivityGroup(BaseModel):
    name = models.CharField(max_length=200)
    poster = models.ImageField(upload_to='activity_groups', blank=True)
    icon = models.FileField(upload_to='activity_groups', blank=True)
    is_diy = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name


class Vendor(BaseModel):
    name = models.CharField(max_length=200)
    logo = models.ImageField(upload_to='vendors', blank=True)
    description = models.TextField(blank=True)
    phone = models.CharField(max_length=200, blank=True, help_text='e.g. 808.293.6020')
    email = models.EmailField(max_length=200, blank=True)
    website = models.URLField(blank=True)
    peek_vendor_id = models.CharField(max_length=200, blank=True,
        help_text='PEEK Vendor ID e.g. "53e2be2fbe29e554a00066c5"')

    def __unicode__(self):
        return self.name


class Activity(BaseModel):
    name = models.CharField(max_length=200)
    tagline = models.CharField(max_length=200, blank=True)
    group = models.ForeignKey(ActivityGroup)
    vendor = models.ForeignKey(Vendor)
    on_site = models.BooleanField(default=True, verbose_name='On-site')
    show_sidebar = models.BooleanField(default=True)
    location = models.CharField(max_length=200, blank=True,
        help_text='Text-based name of the location')
    latitude_longitude = models.CharField(max_length=200, blank=True,
        help_text='Ex: 49.268681, -123.069489')
    map_x = models.IntegerField(blank=True, null=True,
        help_text='X coordinate, in pixels, on the bird-eye map')
    map_y = models.IntegerField(blank=True, null=True,
        help_text='Y coordinate, in pixels, on the bird-eye map')
    details = models.TextField(blank=True)
    recommendations = models.TextField(blank=True,
        help_text='Enter what to bring')
    packages_label = models.CharField(max_length=200, blank=True,
        help_text='Overwrites the "Packages" label if set')
    pdf = models.FileField(upload_to='pdf', blank=True, verbose_name='PDF')
    poster = models.ImageField(upload_to='activities', blank=True)

    def has_packages(self):
        return self.activitypackage_set.count() > 0
    has_packages.boolean = True

    def has_medias(self):
        return self.activitymedia_set.count() > 0
    has_medias.boolean = True

    @property
    def latitude(self):
        if self.latitude_longitude:
            return self.latitude_longitude.split(',')[0]
        else:
            return None

    @property
    def longitude(self):
        try:
            return self.latitude_longitude.split(',')[1]
        except:
            return None

    def __unicode__(self):
        return self.name

    class Meta(BaseModel.Meta):
        verbose_name_plural = 'activities'


class ActivityPackage(BaseModel):
    activity = models.ForeignKey(Activity)
    name = models.CharField(max_length=200)
    short_description = models.TextField(blank=True)
    description = models.TextField(blank=True)
    image = models.ImageField(upload_to='packages', blank=True)
    duration = models.CharField(max_length=200, blank=True)
    peek_activity_id = models.CharField(max_length=200, blank=True,
        help_text='PEEK Activity ID e.g. "53daf232be29e554a0000f05"')
    peek_ticket_id = models.CharField(max_length=200, blank=True,
        help_text='PEEK Ticket ID e.g. "53e2be2fbe29e554a00066c5"')
        
    def has_prices(self):
        return self.activitypackageprice_set.count() > 0
    has_prices.boolean = True

    def __unicode__(self):
        return self.name


class ActivityPackagePrice(BaseModel):
    package = models.ForeignKey(ActivityPackage)
    name = models.CharField(max_length=200)
    price = models.DecimalField(max_digits=12,
                                decimal_places=2,
                                blank=True,
                                null=True,
                                help_text='Digits only')
    price_info = models.CharField(max_length=200, blank=True, help_text='e.g. "per day"')
    description = models.TextField(blank=True)
    
    def __unicode__(self):
        return self.name


class ActivityMedia(BaseModel):
    activity = models.ForeignKey(Activity)
    image = models.ImageField(upload_to='activities')
    youtube_id = models.CharField(max_length=200, blank=True, verbose_name='Youtube ID')
    name = models.CharField(max_length=200)
    caption = models.TextField(blank=True)
    is_featured = models.BooleanField(default=False)

    @property
    def type(self):
        if self.youtube_id:
            return 'video'
        else:
            return 'image'

    def __unicode__(self):
        return self.name


class Media(BaseModel):
    image = models.ImageField(upload_to='medias', help_text='Should be vertical')
    youtube_id = models.CharField(max_length=200, blank=True, verbose_name='Youtube ID')
    name = models.CharField(max_length=200)
    caption = models.TextField(blank=True)

    @property
    def type(self):
        if self.youtube_id:
            return 'video'
        else:
            return 'image'

    def __unicode__(self):
        return self.name


class Event(BaseModel):
    name = models.CharField(max_length=200)
    location = models.CharField(max_length=200, blank=True)
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField(blank=True, null=True)
    description = models.TextField(blank=True)
    poster = models.ImageField(upload_to='events', blank=True)

    def __unicode__(self):
        return self.name


class Screen(models.Model):
    name = models.CharField(max_length=200)
    screensaver = models.ImageField(upload_to='screensavers', blank=True)

    def __unicode__(self):
        return self.name


class Screensaver(BaseModel):
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='screensavers')

    def __unicode__(self):
        return self.name

class Marker(BaseModel):
    name = models.CharField(max_length=200)
    latitude_longitude = models.CharField(max_length=200, blank=True,
        help_text='Ex: 49.268681, -123.069489')
    image_latitude_longitude = models.CharField(max_length=200, blank=True,
        help_text='Coordinates for the visual map')
    activity = models.ForeignKey(Activity, blank=True, null=True)
    icon = models.ImageField(upload_to='markers', blank=True)

    @property
    def latitude(self):
        try:
            return self.latitude_longitude.split(',')[0]
        except:
            return None

    @property
    def longitude(self):
        try:
            return self.latitude_longitude.split(',')[1]
        except:
            return None

    @property
    def image_latitude(self):
        try:
            return self.image_latitude_longitude.split(',')[0]
        except:
            return None

    @property
    def image_longitude(self):
        try:
            return self.image_latitude_longitude.split(',')[1]
        except:
            return None

    def __unicode__(self):
        return self.name