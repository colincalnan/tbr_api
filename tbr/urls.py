from django.conf.urls import patterns, include, url
from tastypie.api import Api
from tbr.api import (ActivityResource, ActivityGroupResource,
    ActivityMediaResource, ScreensaverResource, EventResource, MarkerResource,
    MediaResource)

v1_api = Api(api_name='v1')
v1_api.register(ActivityResource())
v1_api.register(ActivityGroupResource())
v1_api.register(ActivityMediaResource())
v1_api.register(ScreensaverResource())
v1_api.register(EventResource())
v1_api.register(MarkerResource())
v1_api.register(MediaResource())

urlpatterns = patterns('',
    # The normal jazz here...
    url(r'^api/', include(v1_api.urls)),
)