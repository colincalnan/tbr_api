from django.contrib import admin
from tbr.models import (Activity, ActivityGroup, ActivityPackage,
ActivityPackagePrice, ActivityMedia, Event, Vendor, Screen, Screensaver,
Marker, Media)

# Admin inlines

class BaseInline(admin.StackedInline):
    def get_fieldsets(self, request, obj = None):
        res = super(BaseInline, self).get_fieldsets(request, obj)

        # Move default abstract fields to the end
        fields = res[0][1]['fields']
        res[0][1]['fields'] = fields[2:] + fields[:2]
        return res


class ActivityPackageInline(BaseInline):
    model = ActivityPackage

class ActivityPackagePriceInline(BaseInline):
    model = ActivityPackagePrice

class ActivityMediaInline(BaseInline):
    model = ActivityMedia

# Admin models

class BaseAdmin(admin.ModelAdmin):
    def get_fieldsets(self, request, obj = None):
        res = super(BaseAdmin, self).get_fieldsets(request, obj)

        # Move default abstract fields to the end
        fields = res[0][1]['fields']
        res[0][1]['fields'] = fields[2:] + fields[:2]
        return res


class ActivityAdmin(BaseAdmin):
    inlines = [ActivityPackageInline, ActivityMediaInline]
    list_display = ('name', 'group', 'vendor', 'on_site', 'has_packages',
        'has_medias', 'is_active', 'latitude', 'longitude')
    list_filter = ('group', 'vendor', 'on_site', 'is_active')

class ActivityGroupAdmin(BaseAdmin):
    list_display = ('name', 'icon', 'poster', 'is_active')
    list_filter = ('is_active',)

class ActivityPackageAdmin(BaseAdmin):
    inlines = [ActivityPackagePriceInline]
    list_display = ('name', 'activity', 'has_prices')
    list_filter = ('activity',)

class ActivityPackagePriceAdmin(BaseAdmin):
    pass

class ActivityMediaAdmin(BaseAdmin):
    list_display = ('name', 'activity', 'youtube_id', 'is_featured', 'is_active')
    list_filter = ('activity', 'is_featured', 'is_active')

class EventAdmin(BaseAdmin):
    pass

class MediaAdmin(BaseAdmin):
    list_display = ('name', 'youtube_id')

class VendorAdmin(BaseAdmin):
    list_display = ('name', 'logo', 'phone', 'email', 'website')

class ScreensaverAdmin(BaseAdmin):
    list_filter = ('is_active',)

class MarkerAdmin(BaseAdmin):
    list_display = ('name', 'activity', 'latitude', 'longitude',
        'image_latitude', 'image_longitude', 'icon', 'is_active')

admin.site.register(Activity, ActivityAdmin)
admin.site.register(ActivityGroup, ActivityGroupAdmin)
admin.site.register(ActivityPackage, ActivityPackageAdmin)
admin.site.register(ActivityPackagePrice, ActivityPackagePriceAdmin)
admin.site.register(ActivityMedia, ActivityMediaAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Vendor, VendorAdmin)
admin.site.register(Screen)
admin.site.register(Screensaver, ScreensaverAdmin)
admin.site.register(Marker, MarkerAdmin)
admin.site.register(Media, MediaAdmin)